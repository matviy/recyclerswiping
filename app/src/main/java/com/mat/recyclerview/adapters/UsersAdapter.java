package com.mat.recyclerview.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mat.recyclerview.R;
import com.mat.recyclerview.database.model.DbUser;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder>
        implements ItemTouchHelperAdapter {

    private List<DbUser> items;
    private int itemLayout;
    private Callback mCallback;
    Context context;

    private int selectedItem = -1;

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (mCallback != null) {
            mCallback.onItemMoved(items.get(fromPosition), items.get(toPosition));
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(items, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(items, i, i - 1);
                }
            }
            notifyItemMoved(fromPosition, toPosition);
        }
//        notifyDataSetChanged();
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        if (mCallback != null) {
            mCallback.onDismiss(items.get(position));
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public interface Callback {
        void onClick(ViewHolder holder);

        void onItemMoved(DbUser user1, DbUser user2);

        void onDismiss(DbUser user);
    }

    public UsersAdapter(Context context, List<DbUser> items, int itemLayout) {
        this.context = context;
        this.items = items;
        this.itemLayout = itemLayout;
    }

    public void setItems(List<DbUser> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void add(DbUser item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = items.indexOf(item);
        items.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    public DbUser getItem(int pos) {
        return items.get(pos);
    }

    public void move(int pos) {
        Collections.swap(items, pos, 0);
        notifyItemMoved(pos, 0);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        DbUser item = items.get(position);
//        if (selectedItem == position) {
//            holder.root.setBackgroundColor(Color.parseColor("#AA3333"));
//        } else {
//            holder.root.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
//        }
        holder.text.setText(item.getName());
        holder.image.setImageBitmap(null);
        Picasso.with(holder.image.getContext()).cancelRequest(holder.image);
        Picasso.with(holder.image.getContext())
                .load("http://camranger.com/wp-content/uploads/2014/10/Android-Icon.png")
                .fit()
                .centerCrop()
                .into(holder.image);
        holder.itemView.setTag(item);
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mCallback != null) {
//                    mCallback.onClick(position, holder);
//                }
//            }
//        });
        holder.image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mCallback != null) {
//                            setSelectedItem(position);
//                            notifyDataSetChanged();
                            mCallback.onClick(holder);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
//                        setSelectedItem(-1);
//                        notifyDataSetChanged();
                        break;
                }

                return true;
            }
        });
    }

    public void setSelectedItem(int pos) {
        selectedItem = pos;
    }

    public void setCallback(Callback callback) {
        this.mCallback = callback;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
            implements ItemTouchHelperViewHolder {
        public RelativeLayout root;
        public ImageView image;
        public TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            root = (RelativeLayout) itemView.findViewById(R.id.item_root);
            image = (ImageView) itemView.findViewById(R.id.image);
            text = (TextView) itemView.findViewById(R.id.text);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }
}