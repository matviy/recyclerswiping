package com.mat.recyclerview.database.model;

/**
 * Created by MPODOLSKY on 22.06.2015.
 */

public class DbUser {

    public Long _id;
    public long id;
    public String name;

    public DbUser() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
