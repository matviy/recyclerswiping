package com.mat.recyclerview.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import com.mat.recyclerview.database.model.DbUser;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "recycler.db";
    private static final int VERSION = 1;

    private static DatabaseHelper mInstance = null;

    static {
        cupboard().register(DbUser.class);
    }

    public static DatabaseHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return mInstance;
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public static DatabaseHelper getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        cupboard().withDatabase(db).createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        cupboard().withDatabase(db).upgradeTables();
    }

    public void addUser(DbUser user) {
        cupboard().withDatabase(getWritableDatabase()).put(user);
    }

    public DbUser getUser(String email) {
        if (TextUtils.isEmpty(email)) {
            return null;
        } else {
            return cupboard().withDatabase(getReadableDatabase()).query(DbUser.class)
                    .withSelection("email = ?", email)
                    .get();
        }
    }

    public List<DbUser> getUsers() {
        List<DbUser> users = cupboard().withDatabase(getReadableDatabase()).query(DbUser.class)
                .list();
        Collections.sort(users, new Comparator<DbUser>() {
            @Override
            public int compare(DbUser first, DbUser second) {
                if (first.getId() > second.getId()) {
                    return 1;
                }
                if (first.getId() < second.getId()) {
                    return -1;
                }
                return 0;
            }
        });
        return users;
    }

    public void updateUser(DbUser user) {
        ContentValues values = cupboard().withEntity(DbUser.class).toContentValues(user);
        cupboard().withDatabase(getWritableDatabase()).update(DbUser.class, values);
    }

    public void deleteUser(DbUser user) {
        if (user != null) {
            cupboard().withDatabase(getReadableDatabase()).delete(DbUser.class, "id = ?", String.valueOf(user.getId()));
        }
    }

    public void clearUsers() {
        cupboard().withDatabase(getReadableDatabase()).delete(DbUser.class);
    }

}
