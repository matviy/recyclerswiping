package com.mat.recyclerview.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.mat.recyclerview.R;
import com.mat.recyclerview.adapters.SimpleItemTouchHelperCallback;
import com.mat.recyclerview.adapters.UsersAdapter;
import com.mat.recyclerview.database.DatabaseHelper;
import com.mat.recyclerview.database.model.DbUser;
import com.mat.recyclerview.utils.PreferencesManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends ActionBarActivity {

    @Bind(R.id.list)
    RecyclerView mList;
    List<DbUser> mUsers = new ArrayList<>();
    PreferencesManager preferencesManager = PreferencesManager.getInstance();

    UsersAdapter mAdapter;

    final DatabaseHelper dbHelper = DatabaseHelper.getInstance();

    ItemTouchHelper mItemTouchHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mAdapter = new UsersAdapter(this, mUsers, R.layout.item);

        mList.setHasFixedSize(true);


        mList.setAdapter(mAdapter);
        mList.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.ItemAnimator animator = new DefaultItemAnimator();
        animator.endAnimations();
        mList.setItemAnimator(new DefaultItemAnimator());
        ItemTouchHelper.Callback tcallback =
                new SimpleItemTouchHelperCallback(mAdapter);
        mItemTouchHelper = new ItemTouchHelper(tcallback);
        mItemTouchHelper.attachToRecyclerView(mList);
        mAdapter.setCallback(new UsersAdapter.Callback() {
            @Override
            public void onClick(UsersAdapter.ViewHolder holder) {
                mItemTouchHelper.startDrag(holder);
            }

            @Override
            public void onItemMoved(final DbUser user1, final DbUser user2) {
                long tempId1 = user1.getId();
                long tempId2 = user2.getId();
                user1.setId(tempId2);
                user2.setId(tempId1);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        dbHelper.updateUser(user1);
                        dbHelper.updateUser(user2);
                    }
                }).start();
            }

            @Override
            public void onDismiss(DbUser user) {
                dbHelper.deleteUser(user);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUsers();
    }

    @OnClick(R.id.add)
    public void add() {
        int count = preferencesManager.getUsersCount();
        preferencesManager.setUsersCount(count+1);
        DbUser user = new DbUser();
        user.setName("test " + count);
        user.setId(count);
        dbHelper.addUser(user);
        mAdapter.add(user, mAdapter.getItemCount());
    }

    @OnClick(R.id.delete)
    public void delete() {
        updateUsers();
    }

    private void updateUsers() {
        if (mAdapter != null) {
            mAdapter.setItems(dbHelper.getUsers());
        }
    }

}
