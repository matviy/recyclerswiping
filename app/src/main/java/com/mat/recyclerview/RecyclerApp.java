package com.mat.recyclerview;

import android.app.Application;

import com.mat.recyclerview.database.DatabaseHelper;
import com.mat.recyclerview.utils.PreferencesManager;

/**
 * Created by mpodolsky on 09.11.2015.
 */
public class RecyclerApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        PreferencesManager.initializeInstance(this);
        DatabaseHelper.getInstance(this);
    }

}
